package com.example.vaksinasikusehat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText email, password;
    private Button daftar;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        daftar = findViewById(R.id.daftar);
        mAuth = FirebaseAuth.getInstance();
        daftar.setOnClickListener(this);

        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.daftar) {
            daftar();
        } else {
            throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }

    private void daftar() {
        String emailInput = email.getText().toString();
        String passwordInput = password.getText().toString();
        mAuth.createUserWithEmailAndPassword(emailInput, passwordInput)
                .addOnSuccessListener(authResult -> {
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null) {
                        String uid = user.getUid();
                        saveUserData(uid, String.valueOf(email));
                    }
                    nextActivity(true, "Daftar akun berhasil!");
                })
                .addOnFailureListener(authResult -> {
                    nextActivity(false, "Daftar akun gagal, coba lagi!");
                });


    }

    private void nextActivity(boolean isSuccess, String message) {
        Toast.makeText(RegisterActivity.this,message, Toast.LENGTH_SHORT).show();
        if (isSuccess) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void saveUserData(String uid, String email) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users");
        usersRef.child(uid).setValue(email);
    }

}