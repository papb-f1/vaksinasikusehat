package com.example.vaksinasikusehat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageButton logOutBtn;
    LinearLayout cardImunisasi, cardProfil, cardAbout, cardArtikel;
    FirebaseAuth mAuth;
//    FirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // view declaration
        logOutBtn = findViewById(R.id.logout_button);
        cardImunisasi = findViewById(R.id.card_imunisasi);
        cardProfil = findViewById(R.id.card_profil);
        cardAbout = findViewById(R.id.card_about);
        cardArtikel = findViewById(R.id.card_artikel);

        mAuth = FirebaseAuth.getInstance();

        //set onclicklistener on view
        logOutBtn.setOnClickListener(this);
        cardImunisasi.setOnClickListener(this);
        cardProfil.setOnClickListener(this);
        cardAbout.setOnClickListener(this);
        cardArtikel.setOnClickListener(this);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.card_imunisasi) {
            Intent goImunisasi = new Intent(this, DaftarImunisasiActivity.class);
            startActivity(goImunisasi);
        } else if (id == R.id.card_about) {
            Intent goAbout = new Intent(this, AboutActivity.class);
            startActivity(goAbout);
        } else if (id == R.id.logout_button) {
            Intent logout = new Intent(this, LoginActivity.class);
            startActivity(logout);
            mAuth.signOut();
            finish();
        }
    }
}